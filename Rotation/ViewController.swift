//
//  ViewController.swift
//  Rotation
//
//  Created by Kuda Mac 3 on 18/02/2020.
//  Copyright © 2020 Kuda Mac 3. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imsgeView: UIImageView!
    @IBOutlet weak var rotate: UIButton!
    @IBOutlet weak var cardDetailView: UIView!
    @IBOutlet weak var const: NSLayoutConstraint!
    var istapped = false
    
    let frontView = UIImage(named: "card")
    let backView = UIImage(named: "Frame 64")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cardview()
    }
    
    func cardview() {
        imsgeView.image = frontView
        cardDetailView.isHidden = true
        const.constant = 80.0
    }

    @IBAction func rotateBtn(_ sender: UIButton) {
        if istapped{
            istapped = false
            const.constant = 80.0
            cardDetailView.isHidden = true
            rotate.setTitle("Show Details", for: .normal)
            imsgeView.image = frontView
            UIView.transition(with: imsgeView, duration: 1.0, options: .transitionFlipFromLeft, animations: nil, completion: nil)
        }else {
            istapped = true
            const.constant = 25.0
            cardDetailView.isHidden = false
            imsgeView.image = backView
            rotate.setTitle("Hide Details", for: .normal)
            UIView.transition(with: imsgeView, duration: 1.0, options: .transitionFlipFromRight, animations: nil, completion: nil)
        }
       
    }
    
}

